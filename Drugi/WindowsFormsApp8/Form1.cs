﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp8
{
    public partial class Form1 : Form
    {

        private string path = "C:\\Users\\Rerna\\Desktop\\txt.txt";

        private string chosen = "";
        private string copchosen = "";

        private List<string> lista = new List<string>();
        private Random rng = new Random();
        private int broj_Pokusaja;

        public Form1()
        {
            InitializeComponent();
        }

        public void Reset()
        {
            chosen = lista[rng.Next(0, lista.Count - 1)];
            
            for (int i = 0; i < chosen.Length; i++)
            {
                copchosen += "_";
                copchosen += " ";
            }
            follow();
            label3.Text = copchosen;
            
            broj_Pokusaja = 5;      
            label6.Text = broj_Pokusaja.ToString();
            
            label1.Text = chosen;
            

        }
        public void follow()
        {
            label3.Text = "";
            for( int i = 0; i < chosen.Length; i++)
            {
                label3.Text += copchosen.Substring(i, 1);
                label3.Text += " ";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

           
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {

                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    lista.Add(line);
                }
                Reset();
                
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 1)
            {
                if (chosen.Contains(textBox1.Text))
                {
                    char[] temp = copchosen.ToCharArray();
                    char[] find = chosen.ToCharArray();
                    char charac = textBox1.Text.ElementAt(0);
                    for(int i = 0; i < find.Length; i++)
                    {
                        if(find[i] == charac)
                        {
                           temp[i] = charac;
                        }
                    }
                    copchosen = new string(temp);
                    follow();
                   
                    label5.Text += textBox1.Text;
                    label5.Text += " ";

                }
                else
                {
                    --broj_Pokusaja;
                    if (broj_Pokusaja == 0)
                    {
                        MessageBox.Show("GAME OVER!!");
                        DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            Reset();
                        }
                        else
                        {
                            Application.Exit();
                        }
                    }
                    label5.Text += textBox1.Text;
                    label5.Text += " ";

                    label6.Text = broj_Pokusaja.ToString();
                }    
            }else if(textBox1.Text == chosen)
            {
                MessageBox.Show("POBJEDA!!");
            }
            else
            {
                --broj_Pokusaja;
                if (broj_Pokusaja == 0)
                {
                    MessageBox.Show("GAME OVER!!");
                    DialogResult dialogResult = MessageBox.Show("Try Again?", "New Game", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.Yes)
                    {
                        Reset();
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
                label6.Text = broj_Pokusaja.ToString();
            }
            
        }

       
        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
